#SO node, Sistema Operativo ligero
FROM node:12

# Subir archivo al contenedor
COPY package*.json ./

#Validar version de npm
RUN npm -v

#Instalar las librerias de npm
RUN npm install

#Directorio de trabajo. Siguiendo guia https://nodejs.org/de/docs/guides/nodejs-docker-webapp/
WORKDIR /usr/src/app

#Crear usuario y agregarlo a grupo docker
RUN groupadd -r app && useradd -r -g app  app

#Cambiar owner del workdirectory
RUN  chown app:app -R /usr/src/app

#Asignar usuario
USER app

# Copiar el codigo fuente al contenedor
COPY . .

#Correr test unitario
CMD [ "npm", "run", "test" ]

#Exponer puerto
EXPOSE 3000

#Correr aplicacion
CMD [ "node", "index.js" ]